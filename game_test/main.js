enchant();

window.onload = function() {

    var WIDTH = 640;
    var HEIGHT = 400;
    var POSY = 256;
    var UPOSX = 180;
    var EPOSX = 430;
    var RATIOX = 0.05;
    
    
    var core = new Core(WIDTH, HEIGHT);
//    var core = new Core(window.innerWidth, window.innerHeight);
    
  //  var scale_w = window.innerWidth/640;
    //    var scale_h = window.innerHeight/400;
//    core.scale = scale_w;
    
    core.preload(
	'chara1.png',
	'club.png',
	'button.png',
	'back1.png',
	'back2.png',
	'waku.png',
	'life.png',
	'map0.png',
	'lesser.png',
	'heli.png'
    );
    core.fps = 15;
    core.onload = function() {


	var gameScene = new Scene();
	core.replaceScene(gameScene);

	// 背景
	var BackGround = Class.create(Sprite, {
	    initialize: function(x, y) {
		Sprite.call(this, 440, 304);
		this.backgroundColor = "rgb(0, 200, 255)";
		this.x = x;
		this.y = y;
		var maptip = core.assets['map0.png'];
		var image = new Surface(320, 320);
		for (var i = 0; i < 320; i += 16) {
		    image.draw(maptip, 0 * 16, 0, 16, 16, i, 304 - 16, 16, 16);
		    this.image = image;
		}
		gameScene.addChild(this);
	    }
	});
	var b = new BackGround(100, 0);


	var User = Class.create(Sprite, { 
	    initialize: function(x, y) {
		Sprite.call(this, 32, 32);
		this.x = x;
		this.y = y;
		this.image = core.assets['chara1.png'];
		this.pirouette = 1;
		this.sign;
		this.on('enterframe', function() {
		    if (uData.who == 8 && 1 <= this.age % 59 && this.age % 59 <= 14 && uclub1.time < 500) { // 3秒毎に14フレームかけて回る
			if (1 <= this.pirouette) this.sign = '-';
			if (this.pirouette <= -1) this.sign = '+';
			eval('this.pirouette ' + this.sign + '= 0.3;');
			this.scaleX = this.pirouette;
		    }

		    if (uData.who == 8 && this.age % 59 == 14) { // みやた
			uData.cSpeed += 0.4;
			uData.tSpeed += 0.4;
		    }

		    if (this.within(enemy, 70)) { // go
			if (uclub1.time <= 21 && 21 < eclub1.time && eclub1.time < 500) {
			    //label.text = '右の勝ち';
			    uclub1.time = 500;
			    uclub2.time = 500;
			    uclub3.time = 500;
			    this.rotation = -90;
			    this.y = POSY + 10;
			}
			if (21 < uclub1.time && uclub1.time < 500 && eclub1.time <= 21) {
			    //label.text = '左の勝ち';
			    eclub1.time = 500;
			    eclub2.time = 500;
			    eclub3.time = 500;
			    enemy.rotation = 90;
			    enemy.y = POSY + 10;
			}

		    }
		});
		gameScene.addChild(this);
	    },
	    restart: function(x, y) {
		this.frame = 0;
		this.x = x;
		this.y = y;
		this.rotation = 0;
		this.scaleX = 1;
		this.age = 0;
		this.pirouette = 1;
		this.sign = '-';
		if (uData.who == 8) {
		    uData.cSpeed = 3;
		    uData.tSpeed = 5;
		}
	    }	  
	});

	var Enemy = Class.create(Sprite, {
	    initialize: function(x, y) {
		Sprite.call(this, 32, 32);
		this.x = x;
		this.y = y;
		this.image = core.assets['chara1.png'];
		this.frame = 5;
		this.scaleX = -1;
		this.pirouette = -1;
		this.sign;
		this.on('enterframe', function() {
		    if (eData.who == 8 && 1 <= this.age % 59 && this.age % 59 <= 14 && eclub1.time < 500) { // 3秒毎に14フレームかけて回る
			if (1 <= this.pirouette) this.sign = '-';
			if (this.pirouette <= -1) this.sign = '+';
			eval('this.pirouette ' + this.sign + '= 0.3;');
			this.scaleX = this.pirouette;
		    }

		    if (eData.who == 8 && this.age % 59 == 14) { // みやた
			eData.cSpeed += 0.4;
			eData.tSpeed += 0.4;
		    }

		});
		gameScene.addChild(this);
	    },

	    restart: function(x, y) {
		this.frame = 5;
		this.x = x;
		this.y = y;
		this.rotation = 0;
		this.scaleX = -1;
		this.age = 0;
		this.pirouette = -1;
		this.sign = '+';
		if (eData.who == 8) {
		    eData.cSpeed = 3;
		    eData.tSpeed = 5;
		}
	    }
	});



	var Yuma = Class.create(Sprite, { 
	    initialize: function(x, y, uore) {
		Sprite.call(this, 32, 32);
		this.x = x;
		this.y = y;
		this.image = core.assets['chara1.png'];
		if (uore == 'e') {
		    this.frame = 5;
		    this.scaleX = -1;
		}
		this.on('enterframe', function() {
		    if (this.rotation != 0 && this.age % 45 == 2) this.rotation = 0;
		    
		    if (uore == 'u' && uData.who == 9 && this.age % 45 == 0 && uclub1.time < 500) {
			this.rotation = 90;
			uclub4.setpos(this, POSY);
			gameScene.addChild(uclub4);			
		    }
		    if (uore == 'e' && eData.who == 9 && this.age % 45 == 0 && eclub1.time < 500) { 
			this.rotation = -90;
			eclub4.setpos(this, POSY);
			gameScene.addChild(eclub4); 
		    }
		});
	    },
	    restart: function(x, y, r) {
		this.x = x;
		this.y = y;
		this.age = 0;
		this.rotation = 0; // or this.rotation = r;
		this.frame = (r < 0) ? 5 : 0;
	    },
	    reset: function() {
		gameScene.removeChild(this);
	    }
	});
	
	var uYuma = new Yuma(UPOSX-50, POSY, 'u');
	var eYuma = new Yuma(EPOSX+50, POSY, 'e');
	var user = new User(UPOSX-25, POSY);
	var enemy = new Enemy(EPOSX+25, POSY);

	var Club1 = Class.create(Sprite, {
	    initialize: function(x, y, data, opponentData) {
		Sprite.call(this, 32, 32);
		this.data = data;
		this.opponent = opponentData;
		this.sign = data.leftToRight ? 1 : -1;
		this.x = x;
		this.y = y-11;
		this.scaleX *= 0.8;
		this.scaleY *= 0.8;
		this.rotation = -130 * this.sign;
		this.image = core.assets['club.png'];
		this.time = 0;
		this.on('enterframe', function() {
		    if (21 < this.time && this.time < 500 && this.data.who == 7) this.opacity = 0; else this.opacity = 1;// for はやぴー
		    
		    if (this.time == 21) this.time = 0; else this.time += 1;
		    if (this.time <= 2) {
			this.y -= 8;
			this.rotate(-24 * this.sign);
		    } else if (this.time <= 5) {
			this.y -= 5;
			this.rotate(-36 * this.sign);
		    } else if (this.time <= 8) {
			this.y += 5;
			this.rotate(-36 * this.sign);
		    } else if (this.time <= 11) {
			this.y += 8;
			this.rotate(-24 * this.sign);
		    }  else if (this.time <= 16) {
			this.y += 2;
			this.rotate(10 * this.sign);
		    }  else if (this.time <= 21){
			this.y -= 2;
			this.rotate(-10 * this.sign);
		    } else if (100 == this.time) {
			this.y = y - 15;
			this.rotation = -130 * this.sign;
		    } else if (101 <= this.time && this.time <= (101+this.data.time)) {
			this.x += this.data.swipeX;
			this.y -= (this.data.swipeY / this.data.time) * ((101+this.data.time-this.time) / this.data.time);
			this.rotate(-36 * this.sign);
		    } else if ((102+this.data.time) <= this.time && this.time <= (102+2*this.data.time)) {
			this.x += this.data.swipeX;
			this.y += (this.data.swipeY / this.data.time) * ((this.time-102-this.data.time) / this.data.time);
			this.rotate(-36 * this.sign);
		    } else if ((103+2*this.data.time) == this.time) {
			if (this.within(this.data.dummy, this.data.hani)) {
			    //    ubel.text = 'catch';
			    if (500 <= this.opponent.club1.time) {
				this.opponent.life[this.opponent.lifeNum].opacity = 0;
				this.opponent.lifeNum += 1;
				if (this.opponent.lifeNum == 3) {
				    endText.setText(this.data.label + "の勝ち！");
				    core.pushScene(gameOverScene);
				} else {
				    endReason.setTextXY(350, 150, this.data.label + "が生き残った");
				    core.pushScene(preEndScene);
				}
			    } else {
				this.x = this.data.club2.x; // 移動中だと1本ずれる
				this.rotation = -130 * this.sign;
				this.time = 0;
			    }
			} else {
			    //    ubel.text = 'drop';
			    if (this.opponent.club1.time <= 21) {
				this.data.life[this.data.lifeNum].opacity = 0;
				this.data.lifeNum += 1;
				if (this.data.lifeNum == 3) {
				    endText.setText(this.opponent.label + "の勝ち！");
				    core.pushScene(gameOverScene);
				} else {
				    endReason.setTextXY(350, 150, this.data.label + "が落とした");
				    core.pushScene(preEndScene);
				}
			    } else if (this.opponent.club1.time < 500) {
				this.data.club1.time = 500;
				this.data.club2.time = 500;
				this.data.club3.time = 500;
				this.data.character.rotation = -90 * this.sign;
			    } else {
				endReason.setTextXY(350, 150, "両方落とした");
				core.pushScene(preEndScene);
			    }
			}
		    } else if (500 <= this.time) { 
			this.y = 270;
			if (this.opponent.club1.time <= 21){
			    this.data.life[this.data.lifeNum].opacity = 0;
			    this.data.lifeNum += 1;
			    if (this.data.lifeNum == 3) {
				endText.setText(this.opponent.label + "の勝ち！");
				core.pushScene(gameOverScene);
			    } else {
				endReason.setTextXY(320, 150, this.data.label + "が何かにぶつかった");
				core.pushScene(preEndScene);
			    }			    
			} else if (500 <= this.opponent.club1.time){ // go
			    if (this.data.leftToRight) {
				endReason.setTextXY(320, 150, "両方何かにぶつかった");
				core.pushScene(preEndScene);
			    }
			}
		    }
		});
		gameScene.addChild(this);
	    },

	    restart: function(x, y) {
		this.x = x;
		this.y = y - 11;
		this.rotation = -130 * this.sign;
		this.time = 0;
	    },
	    
	    reset: function(x, y) { // go
		for (var i = 0; i < 3; i++) {
		    this.data.life[i].opacity = 1;
		    this.opponent.life[i].opacity = 1;
		}
		this.data.lifeNum = 0;
		this.opponent.lifeNum = 0;
		this.x = x;
		this.y = y - 11;
		this.rotation = -130 * this.sign;
		this.time = 0;
	    }

	});


	var Club2 = Class.create(Sprite, {
	    initialize: function(x, y, data) {
		Sprite.call(this, 32, 32);
		this.data = data;
		this.sign = data.leftToRight ? 1 : -1;
		this.x = x;
		this.y = y-16;
		this.scaleX *= 0.8;
		this.scaleY *= 0.8;
		this.rotation = -58 * this.sign;
		this.image = core.assets['club.png'];
		this.time = 0;
		this.on('enterframe', function() {
		    if (this.time == 21) this.time = 0; else this.time += 1;
		    if (this.time <= 2) {
			this.y += 8;
			this.rotate(-24 * this.sign);
		    } else if (this.time <= 7) {
			this.y += 2;
			this.rotate(10 * this.sign);
		    } else if (this.time <= 12) {
			this.y -= 2;
			this.rotate(-10 * this.sign);
		    } else if (this.time <= 15) {
			this.y -= 8;
			this.rotate(-24 * this.sign);
		    } else if (this.time <= 18) {
			this.y -= 5;
			this.rotate(-36 * this.sign);
		    } else if (this.time <= 21){
			this.y += 5;
			this.rotate(-36 * this.sign);
		    } else if (100 <= this.time && this.time <= (102+2*this.data.time)) {
			this.y = y;
			this.rotate(1000);
		    } else if ((103+2*this.data.time) == this.time) {
			this.y = y - 20;
			this.rotation = -58 * this.sign;
			this.time = 0;
		    } else if (500 <= this.time) {
			this.y = 270;
		    }
		});
		gameScene.addChild(this);
	    },
	    restart: function(x, y) {
		this.x = x;
		this.y = y - 16;
		this.rotation = -58 * this.sign;
		this.time = 0;
	    }
	});

	var Club3 = Class.create(Sprite, {
	    initialize: function(x, y, data) {
		Sprite.call(this, 32, 32);
		this.data = data;
		this.sign = data.leftToRight ? 1 : -1;
		this.x = x;
		this.y = y+10;
		this.scaleX *= 0.8;
		this.scaleY *= 0.8;
		this.rotation = -50 * this.sign;
		this.image = core.assets['club.png'];
		this.time = 0;
		this.on('enterframe', function() {
		    if (this.time == 21) this.time = 0; else this.time += 1;
		    if (this.time == 0) {
			this.y += 2;
			this.rotate(10 * this.sign);
		    } else if (this.time <= 5) {
			this.y -= 2;
			this.rotate(-10 * this.sign);
		    } else if (this.time <= 8) {
			this.y -= 8;
			this.rotate(-24 * this.sign);
		    } else if (this.time <= 11) {
			this.y -= 5;
			this.rotate(-36 * this.sign);
		    } else if (this.time <= 14) {
			this.y += 5;
			this.rotate(-36 * this.sign);
		    } else if (this.time <= 17) {
			this.y += 8;
			this.rotate(-24 * this.sign);
		    } else if (this.time <= 21){
			this.y += 2;
			this.rotate(10 * this.sign);
		    } else if (100 <= this.time && this.time <= (102+2*this.data.time)) {
			this.y = y;
			this.rotate(-100);
		    } else if ((103+2*this.data.time) == this.time) {
			this.y = y + 8;
			this.rotation = -50 * this.sign;
			this.time = 0;
		    } else if (500 <= this.time) {
			this.y = 270;
		    }
		});
		gameScene.addChild(this);
	    },

	    restart: function(x, y) {
		this.x = x;
		this.y = y + 10;
		this.rotation = -50 * this.sign;
		this.time = 0;
	    }
	});

	var Club4 = Class.create(Sprite, {
	    initialize: function(character, y, uore) {
		Sprite.call(this, 32, 32);
		this.sign = (uore == 'u') ? -1 : 1;
		this.x = character.x;
		this.y = y;
		this.scaleX *= 0.8;
		this.scaleY *= 0.8;
		this.rotation = -130 * this.sign;
		this.image = core.assets['club.png'];
		this.time = 0;
		this.on('enterframe', function() {
		    if (this.time == 0) {
			this.y = y;
			this.x = character.x;
			this.rotation = -130 * this.sign;
		    } else if (1 <= this.time && this.time <= 15) {
			this.x -= 10 * this.sign;
			this.y -= 8 / this.time;
			this.rotate(-60 * this.sign);
		    } else if (16 <= this.time && this.time <= 30) {
			this.x -= 10 * this.sign;
			this.y += 8 / (31 - this.time);
			this.rotate(-60 * this.sign);
		    } else if (this.time == 31) {
			this.time = -1;
			this.y = y;
			this.x = character.x;
			gameScene.removeChild(this);
		    }

		    if (uore == 'u' && this.within(enemy, 30) && eclub1.time <= 21) {
			eclub1.time = 500;
			eclub2.time = 500;
			eclub3.time = 500;
			enemy.rotation = 90;
			enemy.y = POSY + 10;
		    }
		    if (uore == 'e' && this.within(user, 30) && uclub1.time <= 21) {
			uclub1.time = 500;
			uclub2.time = 500;
			uclub3.time = 500;
			user.rotation = -90;
			user.y = POSY + 10;
		    }
	    
		    this.time += 1;
		});
	    },
	    setpos: function(character, y) {
		this.y = y;
		this.x = character.x;
	    },
	    reset: function() {
		if (this.time <= 31) {
		    gameScene.removeChild(this);
		}
		this.time = 0;
	    }

	});
	var uclub4 = new Club4(uYuma, POSY, 'u'); // go
	var eclub4 = new Club4(eYuma, POSY, 'e');


	var Dummy = Class.create(Sprite, {
	    initialize: function(x, y) {
		Sprite.call(this, 32, 32);
		this.oposit = 0;
		this.x = x;
		this.y = y;
		gameScene.addChild(this);
	    },
	    reset: function(x, y) {
		this.x = x;
		this.y = y;
	    }
	});
	var uDummy = new Dummy(UPOSX-12, POSY);
	var eDummy = new Dummy(EPOSX+12, POSY);
	
	var Button = Class.create(Sprite, {
	    initialize: function(x, y, r, uoreData) {
		Sprite.call(this, 100, 50);
		this.data = uoreData;
		this.x = x;
		this.y = y;
		this.rotation = r;
		this.image = core.assets['button.png'];
		this.flag = 0;
		gameScene.addChild(this);
		this.on('enterframe', function () {
		    if (this.flag == 1 && this.data.lWall <= this.data.character.x) { // left
			if (this.data.club1.time <= 21) {
			    this.data.character.x -= this.data.cSpeed;
			    this.data.club1.x -= this.data.cSpeed;
			    this.data.club2.x -= this.data.cSpeed;
			    this.data.club3.x -= this.data.cSpeed;
			    this.data.dummy.x -= this.data.cSpeed;
			    this.data.yuma.x -= this.data.cSpeed;
			    this.data.character.frame = (this.data.leftToRight) ? this.age % 3 : this.age % 3 + 5;
			    this.data.yuma.frame = (this.data.leftToRight) ? this.age % 3 : this.age % 3 + 5;
			} else if (this.data.club1.time < 500) {
			    this.data.character.x -= this.data.tSpeed;
			    this.data.club2.x -= this.data.tSpeed;
			    this.data.club3.x -= this.data.tSpeed;
//			    this.data.character.frame = (this.data.leftToRight) ? this.age % 3 : this.age % 3 + 5;
			    this.data.dummy.x -= this.data.tSpeed;
			    this.data.yuma.x -= this.data.tSpeed;
			    this.data.character.frame = (this.data.leftToRight) ? this.age % 3 : this.age % 3 + 5;
			    this.data.yuma.frame = (this.data.leftToRight) ? this.age % 3 : this.age % 3 + 5;
			}
		    } else if (this.flag == 2 && this.data.character.x <= this.data.rWall) { // right
			if (this.data.club1.time <= 21) {
			    this.data.character.x += this.data.cSpeed;
			    this.data.club1.x += this.data.cSpeed;
			    this.data.club2.x += this.data.cSpeed;
			    this.data.club3.x += this.data.cSpeed;
//			    this.data.character.frame = (this.data.leftToRight) ? this.age % 3 : this.age % 3 + 5;
			    this.data.dummy.x += this.data.cSpeed;
			    this.data.yuma.x += this.data.cSpeed;
			    this.data.character.frame = (this.data.leftToRight) ? this.age % 3 : this.age % 3 + 5;
			    this.data.yuma.frame = (this.data.leftToRight) ? this.age % 3 : this.age % 3 + 5;
			} else if (this.data.club1.time < 500) {
			    this.data.character.x += this.data.tSpeed;
			    this.data.club2.x += this.data.tSpeed;
			    this.data.club3.x += this.data.tSpeed;
//			    this.data.character.frame = (this.data.leftToRight) ? this.age % 3 : this.age % 3 + 5;
			    this.data.dummy.x += this.data.tSpeed;
			    this.data.yuma.x += this.data.tSpeed;
			    this.data.character.frame = (this.data.leftToRight) ? this.age % 3 : this.age % 3 + 5;
			    this.data.yuma.frame = (this.data.leftToRight) ? this.age % 3 : this.age % 3 + 5;
			}
		    }

		});
		this.on('touchstart', function (e) {
		    if (e.x < 100 || (440 <= e.x && e.x < 540)) this.flag = 1; else this.flag = 2;
		});
		this.on('touchmove', function (e) {
		    if (e.x < 100 || (440 <= e.x && e.x < 540)) this.flag = 1; else this.flag = 2;
		});
		this.on('touchend', function () {
		    this.flag = 0;
		});

	    }
	});
	
	var Waku = Class.create(Sprite, {
	    initialize: function(x, y) {
		Sprite.call(this, 100, 304);
		this.x = x;
		this.y = y;
		this.opacity = 0; // 非表示
		this.image = core.assets['waku.png'];
		this.on('touchstart', function(e) {
		    if (uclub1.time <= 21 && 0 <= e.x && e.x <= 100 && 0 <= e.y && e.y <= 304) {
			this.on('touchmove', function(e) {
			    if (uclub1.time <= 21 && (e.x <= 1 || 99 <= e.x || e.y <= 1 || 303 <= e.y)) { // 関数にするべき
				uData.swipeX -= e.x;
				uData.swipeY -= e.y;
				
				uData.swipeX = Math.floor(uData.swipeX * -RATIOX);
				uData.swipeY = Math.abs(uData.swipeY) * uData.ratioY;
				uData.time = Math.floor(uData.swipeY / 15) + 2;
				
				uclub1.time = 99;
				uclub2.time = 99;
				uclub3.time = 99;
			    }
			});
			uData.swipeX = e.x;
			uData.swipeY = e.y;
		    } else if (eclub1.time <= 21 && 540 <= e.x && e.x <= 640 && 0 <= e.y && e.y <= 304) {
			this.on('touchmove', function(e) {
			    if (eclub1.time <= 21 && (e.x <= 541 || 639 <= e.x || e.y <= 1 || 303 <= e.y)) {
				eData.swipeX -= e.x;
				eData.swipeY -= e.y;
				
				eData.swipeX = Math.floor(eData.swipeX * -RATIOX);
				eData.swipeY = Math.abs(eData.swipeY) * eData.ratioY;
				eData.time = Math.floor(eData.swipeY / 15) + 2;
				
				eclub1.time = 99;
				eclub2.time = 99;
				eclub3.time = 99;
			    }
			});			
			eData.swipeX = e.x;
			eData.swipeY = e.y;
		    }
		});

		this.on('touchend', function(e) {
		    if (uclub1.time <= 21 && 0 <= e.x && e.x <= 100 && 0 <= e.y && e.y <= 304) {
			uData.swipeX -= e.x;
			uData.swipeY -= e.y;

			uData.swipeX = Math.floor(uData.swipeX * -RATIOX);
			uData.swipeY = Math.abs(uData.swipeY) * uData.ratioY;
			uData.time = Math.floor(uData.swipeY / 15) + 2;

			uclub1.time = 99;
			uclub2.time = 99;
			uclub3.time = 99;
		    } else if (eclub1.time <= 21 && 540 <= e.x && e.x <= 640 && 0 <= e.y && e.y <= 304) {
			eData.swipeX -= e.x;
			eData.swipeY -= e.y;

			eData.swipeX = Math.floor(eData.swipeX * -RATIOX);
			eData.swipeY = Math.abs(eData.swipeY) * eData.ratioY;
			eData.time = Math.floor(eData.swipeY / 15) + 2;

			eclub1.time = 99;
			eclub2.time = 99;
			eclub3.time = 99;

		    }
		});
		gameScene.addChild(this);
	    }
	});

	var Life = Class.create(Sprite, {
	    initialize: function(x, y) {
		Sprite.call(this, 32, 28);
		this.x = x;
		this.y = y;
		this.scaleX *= 0.5;
		this.scaleY *= 0.5;
		this.image = core.assets['life.png'];
		gameScene.addChild(this);
	    }
	});


	var uData = { // go
	    label: "左",
	    swipeX: 0,
	    swipeY: 0,
	    time: 0,
	    lifeNum: 0,
	    life: [],
	    leftToRight: true,
	    character: user,
	    dummy: uDummy,
	    yuma: uYuma,
	    lWall: 104,
	    rWall: 480,	    
	    pos: 25,
	    ratioY: 1.5,
	    hani: 35,
	    cSpeed: 3,
	    tSpeed: 5,
	    who: 0,
	}
	var eData = {
	    label: "右",
	    swipeX: 0,
	    swipeY: 0,
	    time: 0,
	    lifeNum: 0,
	    life: [],
	    leftToRight: false,
	    character: enemy,
	    dummy: eDummy,
	    yuma: eYuma,
	    lWall: 129,
	    rWall: 505,
	    pos: 0,
	    ratioY: 1.5,
	    hani: 35,
	    cSpeed: 3,
	    tSpeed: 5,
	    who: 0,
	}

	for (var i = 0; i < 3; i++) {
	    uData.life[i] = new Life(145-(i*20), 20);
	    eData.life[i] = new Life(460+(i*20), 20);
	}

	function dataReset(data) {
	    data.ratioY = 1.5;
	    data.cSpeed = 3;
	    data.tSpeed = 5;
	    data.hani = 35;
	}
	
	var uclub1 = uData.club1 = new Club1(UPOSX, POSY, uData, /*opponent=*/eData);
	var uclub2 = uData.club2 = new Club2(UPOSX, POSY, uData);
	var uclub3 = uData.club3 = new Club3(UPOSX, POSY, uData);
	var eclub1 = eData.club1 = new Club1(EPOSX, POSY, eData, /*opponent=*/uData);
	var eclub2 = eData.club2 = new Club2(EPOSX, POSY, eData);
	var eclub3 = eData.club3 = new Club3(EPOSX, POSY, eData);


	var uLButton = new Button(0, 305, 180, uData);
	var uRButton = new Button(100, 305, 0, uData);
	var eLButton = new Button(440, 305, 180, eData);
	var eRButton = new Button(540, 305, 0, eData);

	var La = Class.create(Label, { // go
	    initialize: function(x, y, text) {
		Label.call(this);
		this.x = x;
		this.y = y;
		this.text = text;
		this.width = 360;
	    },
	    setText: function(text) {
		this.text = text;
	    },
	    setTextXY: function(x, y, text) {
		this.x = x;
		this.y = y;
		this.text = text;		
	    }
	});


	var uWaku;
	var eWaku;
	var Heli = Class.create(Sprite, {
	    initialize: function(opponent, uore) {
		Sprite.call(this, 83, 39);
		this.image = core.assets['heli.png'];
		this.flag = 1;   
		this.opacity = 0;
		this.scaleX = (uore == 'u') ? -1: 1;
		this.x = (uore == 'u') ? opponent.character.x - 50 : opponent.character.x;
		this.y = 80;

		this.on('enterframe', function() {
		    if (this.age <= 60) { // 4秒
		    	if (this.age == 60) this.x = (uore == 'u') ? opponent.character.x - 50 : opponent.character.x;
			this.opacity = 0; // 非表示
		    } else {
			this.opacity = 1; // 表示
		    }
		    if (120 == this.age) this.age = 0; // 8秒

		    if (this.opacity == 1) {
			if (uore == 'e' && this.x - 40 <= opponent.character.x && opponent.character.x <= this.x + 50) {
			    if (this.flag == 1) {
				gameScene.removeChild(uWaku);
				this.flag = 0;
			    }
			} else if (uore == 'u' && this.x + 10 <= opponent.character.x && opponent.character.x <= this.x + 100) {
			    if (this.flag == 1) {
				gameScene.removeChild(eWaku);
				this.flag = 0;
			    }
			} else if (this.flag == 0) {
			    if (uore == 'u') gameScene.addChild(eWaku);
			    if (uore == 'e') gameScene.addChild(uWaku);
			    this.flag = 1;
			}
		    } else {
			if (this.flag == 0) {
			    if (uore == 'u') gameScene.addChild(eWaku);
			    if (uore == 'e') gameScene.addChild(uWaku);
			    this.flag = 1;			    
			}
		    }
		});		
		gameScene.addChild(this);
	    },
	    restart: function(waku) {
		this.age = 0;
		if (this.flag == 0) {
		    gameScene.addChild(waku);
		    this.flag = 1;
		}
	    },
	    reset: function(waku) {
		gameScene.removeChild(this);
	    }

	});
	var uHeli;
	var eHeli;

	
	var Lesser = Class.create(Sprite, {
	    initialize: function(uore) {
		Sprite.call(this, 32, 32);
		this.image = core.assets['lesser.png'];
		if (uore == 'u') {
		    this.x = 500;
		    this.y = 260;
		    this.scaleX = -1;
		} else {
		    this.x = 110;
		    this.y = 260;
		}
		this.on('enterframe', function() {
		    if (this.age <= 45) { // 5秒
			this.x += (uore == 'u') ? -0.8 : 0.8;
		    } else { // 8秒
			this.x += (uore == 'u') ? 0.5 : -0.5;
		    }
		    if (90 == this.age) this.age = 0;


		    if (uore == 'u' && this.within(enemy, 30) && eclub1.time <= 21) {
			eclub1.time = 500;
			eclub2.time = 500;
			eclub3.time = 500;
			enemy.rotation = 90;
			enemy.y = POSY + 10;
		    }
		    if (uore == 'e' && this.within(user, 30) && uclub1.time <= 21) {
			uclub1.time = 500;
			uclub2.time = 500;
			uclub3.time = 500;
			user.rotation = -90;
			user.y = POSY + 10;
		    }
		});
		gameScene.addChild(this);
	    },
	    restart: function(uore) {
		this.age = 0;
		this.x = (uore == 'u') ? 500 : 110;
	    },
	    reset: function() {
		gameScene.removeChild(this);
	    }
	});
	var uLesser;
	var eLesser;

	var MenuWaku = Class.create(Sprite, {
	    initialize: function() {
		Sprite.call(this, 440, 304);
		this.image = core.assets['back1.png'];
		this.x = 100;
		this.y = 0;
	    }
	});



	
	var CharacterScene = Class.create(Scene, { 
	    initialize: function() {
		Scene.call(this);
		this.addChild(new MenuWaku());
		this.addChild(new La(290, 30, '能力説明'));
		var back = new La(305, 250, '戻る');
		this.addChild(back);
		var charList = ["ふつう：普通",
				"きたじま：力が強い",
				"むらかみ：足が速い",
				"ふれでぃ：キャッチ力が凄い",
				"とぴ：ふれでぃに勝てる",
				"しるびあ：ヘリコプターを出す",
				"たなべ：レッサーパンダがいる",
				"はやぴー：目が悪い",
				"みやた：よく回る",
				"？？？：アルバートが..."];
		for (var i = 0; i < charList.length; i ++) {
		    this.addChild(new La(120+220*(i%2), 80+30*Math.floor(i/2), charList[i]));
		}
		back.on('touchstart', function() {
		    core.popScene(this);
		});
	    }
	});
	var charScene = new CharacterScene();

	var RuleScene = Class.create(Scene, { 
	    initialize: function() {
		Scene.call(this);
		this.addChild(new MenuWaku());
		this.addChild(new La(300, 30, '遊び方'));
		var back = new La(305, 250, '戻る');
		this.addChild(back);
		this.addChild(new La(120, 80, '・'));		
		this.addChild(new La(140, 80, '１台のスマホを２人で操作するゲームです．'));
		for (var i = 0; i < 3; i++)
		    this.addChild(new La(120, 110 + i * 40, '・'));		
		this.addChild(new La(140, 110, '両端の白枠をスワイプするとクラブを投げ，落下地点にキャラクターを移動するとクラブをキャッチします．'));
		this.addChild(new La(140, 150, 'クラブを投げた状態でカスケードしている相手に当たると相手を倒せます．'));
		this.addChild(new La(140, 190, 'それぞれの能力を駆使して相手をやっつけましょう！'));
		back.on('touchstart', function() {
		    core.popScene(this);
		});	
	    }
	});
	var ruleScene = new RuleScene();
	
	var MenuScene = Class.create(Scene, { 
	    initialize: function(data, opponent, text, x, y, uore) {

		var ubear = new Sprite(32, 32);
		ubear.image = core.assets['chara1.png'];
		ubear.scaleX *= 3;
		ubear.scaleY *= 3;
		ubear.x = 150;
		ubear.y = 230;

		var ebear = new Sprite(32, 32);
		ebear.image = core.assets['chara1.png'];
		ebear.frame = 5;
		ebear.scaleX *= -3;
		ebear.scaleY *= 3;
		ebear.x = 460;
		ebear.y = 230;

		Scene.call(this);
		this.addChild(new MenuWaku());
		this.vsText;
		if (uore == 'u') this.addChild(ubear); else this.addChild(ebear);
		var message = new La(200, 30, text);
		var ruleInfo = new La(250, 250, '遊び方');
		var charInfo = new La(350, 250, '能力説明');
		var nameList = ["ふつう", "きたじま", "むらかみ", "ふれでぃ", "とぴ",
				"しるびあ", "たなべ", "はやぴー", "みやた", "らんだむ", "　　　　　　　　　　　　　　　　　"];
		var name = [];
		this.addChild(message);
		this.addChild(charInfo);
		this.addChild(ruleInfo);
		charInfo.on('touchstart', function() {
		    core.pushScene(charScene);
		});
		ruleInfo.on('touchstart', function() {
		    core.pushScene(ruleScene);
		});
		
		for (var i = 0; i < nameList.length; i ++) {
		    if (i == nameList.length - 1) {
			name[i] = new La(200, 30, nameList[i]);
		    } else {
			name[i] = new La(250+100*(i%2), 80+30*Math.floor(i/2), nameList[i]);
		    }
		    this.addChild(name[i]);
		    name[i].on('touchstart', function() {
			data.who = nameList.indexOf(this.text);

			if (data.who == 9) {
			    data.who = rand(9);
			} else if (data.who == 10) {
			    data.who = 9;
			}
			
			
			switch (data.who) {
			case 0: // ふつう
			    this.vsText = (uore == 'u') ? "　　ふつう" : "ふつう";
			    break;			    
			case 1: // きたじま
			    data.ratioY = 5;
			    this.vsText = (uore == 'u') ? "　きたじま" : "きたじま";
			    break;
			case 2: // むらかみ
			    data.cSpeed = 4;
			    data.tSpeed = 6;
			    this.vsText = (uore == 'u') ? "　むらかみ" : "むらかみ";
			    break;
			case 3: // ふれでぃ
			    this.vsText = (uore == 'u') ? "　ふれでぃ" : "ふれでぃ";
			    data.hani = 70;
			    break;			    
			case 4: // とぴ
			    this.vsText = (uore == 'u') ? "　　　とぴ" : "とぴ";
			    break;			    
			case 5: // しるびあ
			    eval(uore+'Heli = new Heli(opponent, uore);');
			    this.vsText = (uore == 'u') ? "　しるびあ" : "しるびあ";
			    break;			    
			case 6: // たなべ
			    eval(uore+'Lesser = new Lesser(uore);'); // go
			    this.vsText = (uore == 'u') ? "　　たなべ" : "たなべ";
			    break;			    
			case 7: // はやぴー
			    this.vsText = (uore == 'u') ? "　はやぴー" : "はやぴー";
			    break;			    
			case 8: // みやた
			    this.vsText = (uore == 'u') ? "　　みやた" : "みやた";
			    break;
			case 9: // ゆりとゆま
			    data.cSpeed = 2;
			    data.tSpeed = 4;
			    eval('gameScene.addChild(' + uore + 'Yuma);');

			    this.vsText = (uore == 'u') ? "ゆりとゆま" : "ゆりとゆま";
			    break;
			}
			
			eval(uore+'VsName = new La(x, y, this.vsText);');
			
			gameScene.addChild(eval(uore+'VsName'));
			core.popScene();
		    });
		}	

	    },
	    reset: function() {
		uVsName.opacity = 0;
		eVsName.opacity = 0;
	    }
	});
	var uMenuScene = new MenuScene(uData, eData, '左のプレイヤーの名前を選んでください', 220, 325, 'u');
	var eMenuScene = new MenuScene(eData, uData, '右のプレイヤーの名前を選んでください', 350, 325, 'e');
	
	var uVsName;
	var eVsName;
	var vs = new La(310, 325, 'VS');
	gameScene.addChild(vs);

	var TouchWaku = Class.create(Sprite, {
	    initialize: function(text, scene) {
		Sprite.call(this, 440, 304);
		this.x = 100;
		this.y = 0;
		this.on('touchstart', function() {
		    switch (text) {
		    case 'pre':
			clean('restart');
			core.popScene(scene);
			break;
		    case 'start':
			if (uData.who == 3 && eData.who == 4) gameScene.removeChild(uWaku);
			if (uData.who == 4 && eData.who == 3) gameScene.removeChild(eWaku);
			core.popScene(scene);
			break;
		    }
		});
		scene.addChild(this);
	    }
	});


	var StartScene = Class.create(Scene, {
	    initialize: function() {
		Scene.call(this);
		var title = new La(640, 130, 'start');
		var vsBlind = new Sprite(230, 50);		
		var startTW = new TouchWaku('start', this);
		uWaku = new Waku(0, 0, uData);
		eWaku = new Waku(540, 0, eData);
		vsBlind.x = 205;
		vsBlind.y = 305;
		vsBlind.image = core.assets['back2.png'];
		this.addChild(vsBlind);
		title.scaleX *= 3;
		title.scaleY *= 3;
		this.addChild(title);
	    },
	    reset: function() {
		if (uData.who == 3 && eData.who == 4) gameScene.addChild(uWaku);
		if (uData.who == 4 && eData.who == 3) gameScene.addChild(eWaku);
	    }
	});	
	var startScene = new StartScene();


	function clean(which) {
	    if (which == 'reset') {
		uclub1.reset(UPOSX, POSY);
		eclub1.reset(EPOSX, POSY);
		startScene.reset();
	    } else if (which == 'restart') {
		uclub1.restart(UPOSX, POSY);
		eclub1.restart(EPOSX, POSY);	
	    }
	    uclub2.restart(UPOSX, POSY);
	    uclub3.restart(UPOSX, POSY);
	    uclub4.reset();
	    eclub2.restart(EPOSX, POSY);
	    eclub3.restart(EPOSX, POSY);
	    eclub4.reset();
	    uYuma.restart(UPOSX-50, POSY, 90);
	    eYuma.restart(EPOSX+50, POSY, -90);	    
	    user.restart(UPOSX-25, POSY);
	    enemy.restart(EPOSX+25, POSY);
	    uDummy.reset(UPOSX-12, POSY);
	    eDummy.reset(EPOSX+12, POSY);
	    if (uData.who == 6) uLesser.restart('u');
	    if (eData.who == 6) eLesser.restart('e');
	    if (uData.who == 5) uHeli.restart(eWaku);
	    if (eData.who == 5) eHeli.restart(uWaku);
	}
	
	var preEndScene = new Scene(); // go
	var preEndTW = new TouchWaku('pre', preEndScene);
	var endReason = new La(350, 150, '');
	endReason.scaleX = 1.5;
	endReason.scaleY = 1.5;
	preEndScene.addChild(endReason);


	var gameOverScene = new Scene();
	var endText = new La(360, 140, '');
	var endTextB = new La(320, 200, 'やめる');
	var endTextC = new La(420, 200, 'もう一度');
	endText.scaleX = 1.5;
	endText.scaleY = 1.5;
	endTextB.scaleX = 1.5;
	endTextB.scaleY = 1.5;
	endTextC.scaleX = 1.5;
	endTextC.scaleY = 1.5;
	gameOverScene.addChild(endText);
	gameOverScene.addChild(endTextB);
	gameOverScene.addChild(endTextC);

	endTextB.on('touchstart', function(e) {
	    clean('reset');
	    if (uData.who == 6) uLesser.reset();
	    if (eData.who == 6) eLesser.reset();
	    if (uData.who == 5) uHeli.reset(eWaku);
	    if (eData.who == 5) eHeli.reset(uWaku);
	    if (uData.who == 9) uYuma.reset();
	    if (eData.who == 9) eYuma.reset();
	    dataReset(uData);
	    dataReset(eData);
	    uMenuScene.reset();
	    eMenuScene.reset();
	    core.popScene(preEndScene);
	    core.pushScene(startScene);
	    core.pushScene(eMenuScene);
	    core.pushScene(uMenuScene);	    
	});
	
	endTextC.on('touchstart', function(e) {
	    clean('reset');
	    core.popScene(preEndScene);
	    core.pushScene(startScene);
	});

	core.pushScene(startScene);
	core.pushScene(eMenuScene);
	core.pushScene(uMenuScene);

    }


    core.start();
};

function rand(n) {
    return Math.floor(Math.random() * (n + 1));
}
